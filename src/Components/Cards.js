import React from 'react';
//import './Person.css';
//import Radium from 'radium';



export const OpenedCard = (props) => {
	const style = {
	  display: 'inline-block',
	  textAlign: 'center',
	  margin: '16px',
	  padding: '16px',
	  border: '1px solid black',
	  backgroundColor: 'white',
	  width: '30px', 
	  height: '30px'
	}

	return (<div style={style}> {props.letter} </div>);
}


export const ClosedCard = (props) => {

	const style = {
	  display: 'inline-block',
	  textAlign: 'center',
	  margin: '16px',
	  padding: '16px',
	  border: '1px solid black',
	  backgroundColor: 'green', 
	  width: '30px',
	  height: '30px',
	  color: 'green'
	}

	return (<div style={style}  onClick={props.clicked}></div>);
}

export const InvisibleCard = (props) => {

	const style = {
	  display: 'inline-block',
	  textAlign: 'center',
	  margin: '16px',
	  padding: '16px',
	  border: '1px white',
	  backgroundColor: 'white',
	  width: '30px', 
	  height: '30px'
	}

	return (<div style={style} > </div>);
}
