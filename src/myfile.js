import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {OpenedCard, ClosedCard} from './Components/Cards';

class App extends Component {

  state = {
    message: '',
    showCards: 'true',
    setOfLetters: [],
    number: 2
  }


  makeSetOfLetters = () => {

    let number = this.state.number;

    const Letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 
      'o', 'p', 'q', 'r', 's', 't', 'u', 'w', 'v', 'x', 'y', 'z'];

    let setOfLetters = [];

    for (let i = 0; i < number; i++) {
      let letter = Letters[Math.floor(Math.random() * Letters.length)];

      const checkIfLetterDoubles = () => {
        let check = false;
        setOfLetters.map( (l) => {
          if (l['letter'] === letter ) {
            check = true;
          }
        });
        return check;
      }

      //здесь скорее всего ошибка в условии проверки что буква повторяется: если она дейтсивтельно повтоярится то в массиве будет не достаточное число элементов
      while ((setOfLetters.length < number) && (!checkIfLetterDoubles())) {
          setOfLetters.push({letter: letter, isShown: false});
      }
    }

    setOfLetters.push(...setOfLetters);
    console.log('set of letters:'); console.log(setOfLetters);

    this.setState({
      setOfLetters: [2, 3, 5]
    })

        console.log('this set of letters:'); console.log(this.state.setOfLetters);
  }

  closeThisCardHandler() {
    return null;
  }

  render () {

    let cards = [];

    const buttonStyle = {
      backgroundColor: 'white',
      color: 'black',
      font: 'inherit',
      border: '4px solid black',
      padding: '8px',
      cursor: 'pointer',
      width: '200px'
    }


    if (this.state.number >= 2) {
      cards = (
        <div>
              {this.state.setOfLetters.map((letter, index) => {
                return (
                  <OpenedCard clicked={this.closeThisCardHandler(index)} letter={letter[letter]} />
                  );}
              )}
        </div>
      );

      this.state.message = 'Try it one more time!';
    }

    console.log(cards);

    return (
      <div className = 'App'>
        <header >
          <p> Memo Letters Game</p>
         </header>
        <div>
          <p> This is amazing memo game with letters</p>
          <p> <button
          style={buttonStyle}
          onClick = {() => this.makeSetOfLetters()}
          > Go! </button></p>
        </div>
        <div>
            {cards}
        </div>
        <p> </p>
        <div><p>{this.state.message}</p></div>
      </div>
    );
  }
}

export default App;
