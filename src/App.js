import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {OpenedCard, ClosedCard, InvisibleCard} from './Components/Cards';

class App extends Component {

  state = {
    message: '',
    showCards: 'true',
    setOfLetters: [],
    number: 4,
    win: false,
    openedLetters: {letter1: '', index1: null, letter2: '', index2: null, count: 0}
  }


  makeSetOfLetters = () => {

    let number = this.state.number;
    const Letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
      'o', 'p', 'q', 'r', 's', 't', 'u', 'w', 'v', 'x', 'y', 'z'];
    let setOfLetters = [];

    for (let i = 0; i < number; i++) {
      let id = 1;
      let letter =  Letters[Math.floor(Math.random() * Letters.length)];

       setOfLetters.push({id: id, letter: letter, isShown: 'close'});
       id++;
       setOfLetters.push({id: id, letter: letter, isShown: 'close'});
       Letters.splice(Letters.indexOf(letter), 1);
     //  console.log('Letters = ' + Letters);
    }

    let shuffledSetOfLetters = setOfLetters
      .map((a) => ({sort: Math.random(), value: a}))
      .sort((a, b) => a.sort - b.sort)
      .map((a) => a.value)

    this.setState({
      setOfLetters: shuffledSetOfLetters
    })
  }

  closeThisCardHandler(index) {
    let setOfLetters = [...this.state.setOfLetters];
    let openedLetters = {...this.state.openedLetters};



    if (openedLetters.count == 2) {
      setOfLetters[openedLetters.index1].isShown = 'close';
      setOfLetters[openedLetters.index2].isShown = 'close';
      openedLetters = {letter1: '', index1: null, letter2: '', index2: null, count: 0}
    }

    else {
      setOfLetters[index].isShown = 'open';

      if (openedLetters.letter1 === setOfLetters[index].letter) {
        //setTimeout (function() {
        setOfLetters[index].isShown = 'hide';
        setOfLetters[openedLetters.index1].isShown = 'hide';
        openedLetters = {letter1: '', index1: null, letter2: '', index2: null, count: 0}
      //}, 500)
      }
      else if (openedLetters.count == 0) {
        openedLetters = {letter1: setOfLetters[index].letter, index1: index, letter2: '', index2: null, count: 1}
      }
      else if (openedLetters.count == 1) {
        openedLetters.letter2 = setOfLetters[index].letter;
        openedLetters.index2 = index;
        openedLetters.count++;
      }
    }


    this.setState({
      setOfLetters: setOfLetters,
      openedLetters: openedLetters,
    });
    console.table(setOfLetters);

  }

  render () {

    let cards = [];

    if (this.state.number >= 2) {


      console.log("set of L = " + JSON.stringify(this.state.setOfLetters));

      let numberInARow = this.state.number;
      let firstHalf = this.state.setOfLetters.slice(0, numberInARow);
      let secondHalf =this.state.setOfLetters.slice(numberInARow, numberInARow*2);

  //          console.log("set of L first = " + JSON.stringify(firstHalf));

  //                console.log("set of L second = " + JSON.stringify(secondHalf));
      cards = (
        <div> 

        <div>
              {firstHalf.map((letter, index) => {
                   if (letter['isShown'] === 'open') {
                    return (
                      <OpenedCard
                      letter={letter['letter']} />
                      );
                  }
                  else if (letter['isShown'] === 'close')  {
                    return (
                      <ClosedCard
                      letter={letter['letter']}
                      clicked={() => this.closeThisCardHandler(index)} />
                    );
                  }
                  else if (letter['isShown'] === 'hide')  {
                    return (
                      <InvisibleCard letter={letter['letter']} />
                    );
                  }
               })
              }
        </div>

        <div>
              {secondHalf.map((letter, index) => {
                   if (letter['isShown'] === 'open') {
                    return (
                      <OpenedCard
                      letter={letter['letter']} />
                      );
                  }
                  else if (letter['isShown'] === 'close')  {
                    return (
                      <ClosedCard
                      letter={letter['letter']}
                      clicked={() => this.closeThisCardHandler(index+numberInARow)} />
                    );
                  }
                  else if (letter['isShown'] === 'hide')  {
                    return (
                      <InvisibleCard letter={letter['letter']} />
                    );
                  }
               })
              }
        </div>

        </div>

      );

/*
    this.setState({
      message: 'Try it one more time!'
    });
*/
    this.state.message = 'Try it one more time!';

    }

    return (
      <div className = 'App'>
        <header >
          <p> Memo Letters Game</p>
          <p> Теперь через ДЖенкинс!</p>
         </header>
        <div>
          <p> This is amazing memo game with letters</p>
          <p> <button
          onClick = {() => this.makeSetOfLetters()}
          > Go! </button></p>
        </div>
        <div>
            {cards}
        </div>
        <p> </p>
        <div><p>{this.state.message}</p></div>
      </div>
    );
  }
}

export default App;
